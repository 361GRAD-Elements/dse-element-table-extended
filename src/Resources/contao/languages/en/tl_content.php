<?php

/**
 * 361GRAD Element Table Extended
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']            = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_tableextended_start'] = ['Extended Table Wrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_tableextended_entry'] = ['Extended Table Entry', ''];
$GLOBALS['TL_LANG']['CTE']['dse_tableextended_stop']  = ['Extended Table Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_secondline']  =
    ['Headline (Line 2)', 'Here you can add a second line to the headline.'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Subheadline', 'Here you can add a Subheadline.'];

$GLOBALS['TL_LANG']['tl_content']['switchlayout_legend']            = 'First Column Mode';
$GLOBALS['TL_LANG']['tl_content']['tableextended_legend']           = 'Extended Table';
$GLOBALS['TL_LANG']['tl_content']['tableextendedheadrow_legend']    = 'Extended Table Head Row';
$GLOBALS['TL_LANG']['tl_content']['tableextendedcontentrow_legend'] = 'Extended Table Content Row';

$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_head'] = ['Table Head Row', 'Enter head row items.'];

$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_firstimage'] = ['Column 1 Image', 'Choose Image in first column.'];
$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_firsttext']  = ['Column 1 Text', 'Enter Text for first column.'];
$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_entry']      = ['Content Row', 'Define single columns content.'];

$GLOBALS['TL_LANG']['tl_content']['ts_col_1'] = ['Column 1'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_2'] = ['Column 2'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_3'] = ['Column 3'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_4'] = ['Column 4'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_5'] = ['Column 5'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_6'] = ['Column 6'];

$GLOBALS['TL_LANG']['tl_content']['switchlayout'] = [
    'Mode',
    'Please choose the mode.',
    'imagemode' => 'Image',
    'textmode'  => 'Text',
];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Margin Settings';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Margin Top', 'Here you can add Margin to the top edge of the element (numbers only)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Margin Bottom', 'Here you can add Margin to the bottom edge of the element (numbers only)'];
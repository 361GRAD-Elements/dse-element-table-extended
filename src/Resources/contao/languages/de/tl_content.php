<?php

/**
 * 361GRAD Element Table Extended
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']            = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_tableextended_start'] = ['Erweiterte Tabelle Wrapper Start', ''];
$GLOBALS['TL_LANG']['CTE']['dse_tableextended_entry'] = ['Erweiterte Tabelle Eintrag', ''];
$GLOBALS['TL_LANG']['CTE']['dse_tableextended_stop']  = ['Erweiterte Tabelle Wrapper Stop', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_secondline']  =
    ['Überschrift (Zeile 2)', 'Fügt einer Überschrift eine zweite Zeile hinzu'];
$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'] =
    ['Unterüberschrift', 'Fügt eine Subheadline hinzu'];

$GLOBALS['TL_LANG']['tl_content']['switchlayout_legend']            = 'Modus erste Spalte';
$GLOBALS['TL_LANG']['tl_content']['tableextended_legend']           = 'Erweiterte Tabelle ';
$GLOBALS['TL_LANG']['tl_content']['tableextendedheadrow_legend']    = 'Erweiterte Tabelle Kopfzeile';
$GLOBALS['TL_LANG']['tl_content']['tableextendedcontentrow_legend'] = 'Erweiterte Tabelle Inhaltszeile';

$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_head'] = ['Kopfzeile', 'Geben Sie die Spaltenüberschriften ein.'];

$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_firstimage'] = ['Spalte 1 Bild', 'Bild für erste Spalte.'];
$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_firsttext']  = ['Spalte 1 Text', 'Text für erste Spalte.'];
$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_entry']      = ['Inhaltszeile', 'Geben Sie Spalteninhalt ein.'];

$GLOBALS['TL_LANG']['tl_content']['ts_col_1'] = ['Spalte 1'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_2'] = ['Spalte 2'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_3'] = ['Spalte 3'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_4'] = ['Spalte 4'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_5'] = ['Spalte 5'];
$GLOBALS['TL_LANG']['tl_content']['ts_col_6'] = ['Spalte 6'];

$GLOBALS['TL_LANG']['tl_content']['switchlayout'] = [
    'Modus',
    'Bitte Modus wählen.',
    'imagemode' => 'Bild',
    'textmode'  => 'Text',
];

$GLOBALS['TL_LANG']['tl_content']['margin_legend']   = 'Randeinstellungen';
$GLOBALS['TL_LANG']['tl_content']['dse_marginTop']   = ['Rand oben', 'Hier können Sie Margin zum oberen Rand des Elements hinzufügen (nur nummern)'];
$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom']   = ['Rand unten', 'Hier können Sie dem unteren Rand des Elements Rand hinzufügen (nur nummern)'];
<?php

/**
 * 361GRAD Element Table Extended
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_CTE']['dse_elements']['dse_tableextended_start'] =
    'Dse\\ElementsBundle\\ElementTableExtended\\Element\\ContentDseTableExtendedStart';
$GLOBALS['TL_CTE']['dse_elements']['dse_tableextended_entry'] =
    'Dse\\ElementsBundle\\ElementTableExtended\\Element\\ContentDseTableExtendedEntry';
$GLOBALS['TL_CTE']['dse_elements']['dse_tableextended_stop']  =
    'Dse\\ElementsBundle\\ElementTableExtended\\Element\\ContentDseTableExtendedStop';

$GLOBALS['TL_WRAPPERS']['start'][] = 'dse_tableextended_start';
$GLOBALS['TL_WRAPPERS']['stop'][]  = 'dse_tableextended_stop';

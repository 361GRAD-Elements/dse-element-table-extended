<?php

/**
 * 361GRAD Element Table Extended
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

// Element palettes
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_tableextended_start']          =
    '{type_legend},type,headline,dse_secondline,dse_subheadline;' .
    '{tableextendedheadrow_legend},dse_tableextended_head;' .
    '{margin_legend},dse_marginTop,dse_marginBottom;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests,cssID;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_tableextended_entry']          =
    '{type_legend},type;' .
    '{switchlayout_legend},dse_switchlayout;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_tableextended_entryimagemode'] =
    '{type_legend},type;' .
    '{switchlayout_legend},dse_switchlayout;' .
    '{tableextendedcontentrow_legend},dse_tableextended_firstimage,dse_tableextended_firstimage_alt,' .
        'dse_tableextended_firstimage_size,dse_tableextended_entry;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_tableextended_entrytextmode']  =
    '{type_legend},type;' .
    '{switchlayout_legend},dse_switchlayout;' .
    '{tableextendedcontentrow_legend},dse_tableextended_firsttext,dse_tableextended_entry;' .
    '{invisible_legend:hide},invisible,start,stop';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dse_tableextended_stop']           =
    '{type_legend},type;' .
    '{protected_legend:hide},protected;' .
    '{expert_legend:hide},guests;' .
    '{invisible_legend:hide},invisible,start,stop';


$GLOBALS['TL_DCA']['tl_content']['palettes']['__selector__'][] = 'dse_switchlayout';

// Element fields
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_switchlayout'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['switchlayout'],
    'inputType' => 'select',
    'options'   => ['imagemode', 'textmode'],
    'reference' => &$GLOBALS['TL_LANG']['tl_content']['switchlayout'],
    'eval'      => [
        'includeBlankOption' => true,
        'submitOnChange'     => true,
        'tl_class'           => 'w50'
    ],
    'sql'       => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_secondline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_secondline'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_subheadline'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_subheadline'],
    'search'    => true,
    'inputType' => 'inputUnit',
    'options'   => [
        'h2',
        'h3',
        'h4',
        'h5',
        'h6'
    ],
    'eval'      => [
        'maxlength' => 200,
        'tl_class'  => 'w50'
    ],
    'sql'       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tableextended_head'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_head'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'maxCount'     => 1,
        'minCount'     => 1,
        'buttons'      => ['copy' => false, 'delete' => false, 'up' => false, 'down' => false],
        'columnFields' => [
            'ts_col_1' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_1'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_2' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_2'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_3' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_3'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_4' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_4'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_5' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_5'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_6' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_6'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
        ]
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tableextended_firsttext'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_firsttext'],
    'inputType' => 'text',
    'search'    => true,
    'eval'      => [
        'maxlenght' => 200,
        'tl_class'  => 'w50',
    ],
    'sql'       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tableextended_firstimage'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_firstimage'],
    'inputType' => 'fileTree',
    'eval'      => [
        'filesOnly'  => true,
        'fieldType'  => 'radio',
        'extensions' => 'jpg,jpeg,png,gif,svg',
        'style'      => 'width:200px;'
    ],
    'sql'       => 'binary(16) NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tableextended_firstimage_alt'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['alt'],
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50 clr',
    ],
    'sql'       => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tableextended_firstimage_size'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['size'],
    'inputType' => 'imageSize',
    'options'   => System::getContainer()->get('contao.image.image_sizes')->getAllOptions(),
    'reference' => &$GLOBALS['TL_LANG']['MSC'],
    'eval'      => [
        'rgxp'               => 'digit',
        'includeBlankOption' => true,
        'tl_class'           => 'w50',
    ],
    'sql'       => "varchar(64) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_tableextended_entry'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_tableextended_entry'],
    'exclude'   => true,
    'inputType' => 'multiColumnWizard',
    'eval'      => [
        'tl_class'     => 'clr',
        'columnFields' => [
            'ts_col_2' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_2'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_3' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_3'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_4' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_4'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_5' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_5'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
            'ts_col_6' => [
                'label'     => &$GLOBALS['TL_LANG']['tl_content']['ts_col_6'],
                'exclude'   => true,
                'inputType' => 'text',
                'eval'      => ['style' => 'width:135px']
            ],
        ]
    ],
    'sql'       => "blob NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginTop'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginTop'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
$GLOBALS['TL_DCA']['tl_content']['fields']['dse_marginBottom'] = [
    'label'     => &$GLOBALS['TL_LANG']['tl_content']['dse_marginBottom'],
    'search'    => true,
    'inputType' => 'text',
    'eval'      => [
        'tl_class' => 'w50'
    ],
    'sql'       => "text NULL"
];
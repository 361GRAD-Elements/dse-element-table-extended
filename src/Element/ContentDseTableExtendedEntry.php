<?php

/**
 * 361GRAD Element Table Extended
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementTableExtended\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\Database;
use Contao\File;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\StringUtil;
use Exception;

/**
 * Class ContentDseTableExtendedEntry
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseTableExtendedEntry extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_tableextended_entry';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->title = $this->headline;

            $arrTableRow = StringUtil::deserialize($this->dse_tableextended_entry);

            $values = '';
            foreach ($arrTableRow as $row) {
                $values .= implode(" | ", $row) . "<br>";
            }

            $objTemplate->wildcard = $values;

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return boolean
     */
    protected function compile()
    {
        // Get parent starting wrapper with table head data
        $db = Database::getInstance()->prepare('SELECT * FROM tl_content WHERE pid=? AND type=? AND sorting<? ORDER BY sorting DESC');

        $objResult = $db->execute($this->pid, 'dse_tableextended_start', $this->sorting);

        $tableHead = $objResult->dse_tableextended_head;

        // Extract serialized data from table head
        $arrTableHead              = StringUtil::deserialize($tableHead);
        $this->Template->tableHead = $arrTableHead[0];

        // Insert Image Object to Template
        $self = $this;

        $this->Template->getImageObject = function () use ($self) {
            return call_user_func_array(array($self, 'getImageObject'), func_get_args());
        };

        // Extract serialized data from table row
        $arrTableRow = StringUtil::deserialize($this->dse_tableextended_entry);

        $arrTableRow = $this->removeLastEmptyArrayFields($arrTableRow);

        $this->Template->tableRow = $arrTableRow;

        return true;
    }

    /**
     * Get an image object from uuid
     *
     * @param       $uuid
     * @param null  $size
     * @param null  $maxSize
     * @param null  $lightboxId
     * @param array $item
     *
     * @return \FrontendTemplate|object
     */
    public function getImageObject($uuid, $size = null, $maxSize = null, $lightboxId = null, $item = array())
    {
        global $objPage;

        if (!$uuid) {
            return null;
        }

        $image = FilesModel::findByUuid($uuid);

        if (!$image) {
            return null;
        }

        try {
            $file = new File($image->path, true);
            if (!$file->exists()) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }

        $imageMeta = $this->getMetaData($image->meta, $objPage->language);

        if (is_string($size) && trim($size)) {
            $size = deserialize($size);
        }
        if (!is_array($size)) {
            $size = array();
        }
        $size[0] = isset($size[0]) ? $size[0] : 0;
        $size[1] = isset($size[1]) ? $size[1] : 0;
        $size[2] = isset($size[2]) ? $size[2] : 'crop';

        $image = array(
            'id'        => $image->id,
            'uuid'      => isset($image->uuid) ? $image->uuid : null,
            'name'      => $file->basename,
            'singleSRC' => $image->path,
            'size'      => serialize($size),
            'alt'       => $imageMeta['title'],
            'imageUrl'  => $imageMeta['link'],
            'caption'   => $imageMeta['caption'],
        );

        $image = array_merge($image, $item);

        $imageObject = new FrontendTemplate('dse_image_object');
        $this->addImageToTemplate($imageObject, $image, $maxSize, $lightboxId);
        $imageObject = (object) $imageObject->getData();

        if (empty($imageObject->src)) {
            $imageObject->src = $imageObject->singleSRC;
        }

        return $imageObject;
    }

    /**
     * removeLastEmptyArrayFields removes the last array item of an array list
     *
     * @param $a    array Array with list of arrays which want to get last items killed
     *
     * @return mixed
     */
    private function removeLastEmptyArrayFields($a)
    {
        // Loop over all single arrays in the big one.
        foreach ($a as $i => $c) {
            // reverse complete array
            $ra = array_reverse($c, true);
            // loop through reversed array so last items are first now
            foreach ($ra as $k => $v) {
                if ($v == '') {
                    // kill "empty" items out of origin array
                    unset($a[$i][$k]);
                } else {
                    // we just want to kill the last ones, so break if we don't get an empty one
                    break;
                }
            }
        }
        return $a;
    }
}

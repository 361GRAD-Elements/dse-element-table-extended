<?php

/**
 * 361GRAD Element Table Extended
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementTableExtended\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\StringUtil;

/**
 * Class ContentDseTableExtendedStart
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseTableExtendedStart extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_tableextended_start';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $this->strTemplate = 'be_wildcard';
            $objTemplate       = new BackendTemplate($this->strTemplate);

            return $objTemplate->parse();
        }

        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        // Build subheadline like Contao headline
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';

        // Extract serialized data of table head
        $arrTableHead = StringUtil::deserialize($this->dse_tableextended_head);

        // There is only 1 we need
        $arrTableHead = $arrTableHead[0];

        // Reverse array so we can handle the last fields
        $reversedArrTableHead = array_reverse($arrTableHead, true);

        // Loop through fields and kick off every field with no content til item occurs with content, then stop
        foreach ($reversedArrTableHead as $key => $item) {
            if ($item == '') {
                unset($arrTableHead[$key]);
            } else {
                break;
            }
        }

        // And at last, push it in template vars
        $this->Template->tableHead = $arrTableHead;
    }
}
